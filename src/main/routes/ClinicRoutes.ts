import {Request, Response} from 'express';
import SchedulesDao from '../daos/SchedulesDao';
import scheduleController from '../controllers/schedule.controller';

export class Routes {
    private schedulesDao: SchedulesDao;

    constructor (schedulesDao: SchedulesDao) {
        this.schedulesDao = schedulesDao;
    }

    public routes(app: any): void {

        app.route('/')
        .get((req: Request, res: Response) => {
            res.status(200).send(scheduleController.GET_DEFAULT());
        });

        // Schedules
        app.route('/schedules')
        // GET endpoint
        .get((req: Request, res: Response) => {
        // Get all schedules
            console.log(this.schedulesDao.findAll());
            res.status(200).send({
                schedules : [this.schedulesDao.findAll()],
            });
        })
        // POST endpoint
        .post((req: Request, res: Response) => {
        // Create new schedules
            this.schedulesDao.save(req.body);
            res.status(200).send({
                message: 'POST request successfulll!!!!',
            });
        });

        // Schedules detail
        app.route('/schedules/:schedulesId')
        // get specific schedules
        .get((req: Request, res: Response) => {
        // Get a single schedules detail
            res.status(200).send({
                message: 'GET request successfulll!!!!',
            });
        })
        .delete((req: Request, res: Response) => {
        // Delete a schedules
            this.schedulesDao.remove(req.params.schedulesId);
            res.status(200).send({
                message: 'DELETE request successfulll!!!!',
            });
        });
    }
}
