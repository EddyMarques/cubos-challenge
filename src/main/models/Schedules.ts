export default class Schedules {
    public static FROMJSON(json: string) {
        const obj = JSON.parse(json);
        return new Schedules(obj._day, obj._isDaily, obj._isWeekly);
    }
    private _day: Date;
    private _isDaily: boolean;
    private _isWeekly: boolean;

    constructor(day: Date, isDaily: boolean, isWeekly: boolean) {
        this._day = day;
        this._isDaily = isDaily;
        this._isWeekly = isWeekly;
    }

    public get day(): Date {
        return this._day;
    }

    public get isDaily(): boolean {
        return this._isDaily;
    }

    public get isWeekly(): boolean {
        return this._isWeekly;
    }

    public toJson() {
        return JSON.stringify(new Schedules(this._day, this._isDaily, this._isWeekly));
    }
}