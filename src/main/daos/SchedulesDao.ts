import low from 'lowdb';
import FileSync from 'lowdb/adapters/FileSync';
import { generate } from 'shortid';

const adapter = new FileSync('db.json');
const db = low(adapter);

export default class SchedulesDao {
    constructor() {
        db.defaults({ schedules: [] })
        .write();
    }

    public findAll() {
        return db.get('schedules')
        .value();
    }

    public save(schedules: JSON) {
        db.get('schedules')
        .push(schedules)
        .last()
        .assign({ id: generate()})
        .write();
    }

    public remove(scheduleId: BigInteger) {
        db.get('schedules')
        .remove({id: scheduleId})
        .write();
    }

}
