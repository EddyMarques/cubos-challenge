import * as bodyParser from 'body-parser';
import express from 'express';
import { Routes } from './main/routes/ClinicRoutes';
import SchedulesDao from './main/daos/SchedulesDao';

class App {

    public app: express.Application;
    public route: Routes = new Routes(new SchedulesDao());

    constructor() {
        this.app = express();
        this.config();
        this.route.routes(this.app);
    }

    private config(): void {
        // support application/json type post data
        this.app.use(bodyParser.json());
        // support application/x-www-form-urlencoded post data
        this.app.use(bodyParser.urlencoded({ extended: false }));
    }

}

export default new App().app;
