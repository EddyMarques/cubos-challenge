import { expect } from 'chai';
import Schedules from '../../main/models/Schedules';

describe('[Schedules]', () => {
    it('Should convert object to json', () => {
        const schedules = new Schedules(new Date('2010-10-10'), false, true);
        const scheduleJson = schedules.toJson();

        expect(scheduleJson).to.contain('\"_isDaily\":false');
        expect(scheduleJson).to.contain('\"_isWeekly\":true');
        expect(scheduleJson).to.contain('\"_day\":\"2010-10-10T00:00:00.000Z\"');
    });

    it('Should convert from json to Object', () => {
        const schedules = '{\"_day\":\"2019-04-13T15:14:50.477Z\",\"_isDaily\":false,\"_isWeekly\":true}';
        const object = Schedules.FROMJSON(schedules);

        expect(object.day).to.be.equal('2019-04-13T15:14:50.477Z');
        expect(object.isDaily).to.be.equal(false, 'should be false');
        expect(object.isWeekly).to.be.equal(true, 'should be true');
    });
});
